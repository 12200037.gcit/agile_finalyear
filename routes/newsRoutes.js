const express = require('express')
const newsFeedsController = require('./../controllers/newsFeedsController')
const authController = require('./../controllers/authController')
const router = express.Router()

router
    .route('/')
    .get(newsFeedsController.getAllNews)
    .post(authController.protect, authController.restrictTo('sme'), newsFeedsController.createNews)

router
    .route('/:id')
    .get(newsFeedsController.getNews)
    .patch(newsFeedsController.updateNews)
    .delete(newsFeedsController.deleteNews)

module.exports = router